`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.02.2017 12:12:11
// Design Name: 
// Module Name: beispiel1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module beispiel1(
    input a,
    input b,
    output c,
    output s
    );    
    assign s = a ^ b; // exklusiv-oder
    assign c = a & b; // und
endmodule