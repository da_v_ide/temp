//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.02.2017 12:16:51
// Design Name: 
// Module Name: testbed
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

`timescale 1ns / 10ps //timescale <reference_time> / <precision>
module testbed();
    logic a, b;
    logic c, s;
    
    beispiel1 tester1(.a(a), .b(b), .c(c), .s(s));
    
    initial begin
        a = 0; b = 0; #1;
        a = 1; b = 0; #1;
        a = 1; b = 1;
    end

endmodule
